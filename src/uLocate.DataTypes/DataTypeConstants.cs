﻿namespace uLocate.DataTypes
{
    /// <summary>
    /// Defines the Guids for all the DataTypes. e.g. Maybe someday there will be 3rd-party developers.
    /// </summary>
    public static class DataTypeConstants
    {
        /// <summary>
        /// Guid for the GeocodedAddress data-type.
        /// </summary>
        public const string GeocodedAddressId = "FEF9A214-1429-46E9-BB0C-09CEF3787900";


        /// <summary>
        /// Guid for the AutoGeographyPoint data-type.
        /// </summary>
        public const string AutoGeographyPointId = "AB4B996B-2876-4B0F-8EED-80289C100BAC";
    }
}