﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;
using umbraco.cms.businesslogic;
using umbraco.cms.businesslogic.property;

namespace uLocate.DataTypes
{
    public class uLocateApplicationBase : ApplicationBase
    {
        public uLocateApplicationBase()
        {
            Document.BeforeSave += new Document.SaveEventHandler(Document_BeforeSave);
        }

        private void Document_BeforeSave(Document sender, SaveEventArgs e)
        {
            var geoProps = sender.GenericProperties.Where(p => Guid.Equals(p.PropertyType.DataTypeDefinition.UniqueId, new Guid(DataTypeConstants.GeographySetAddressId)));
            if (geoProps.Count() > 0)
            {
                foreach (var p in geoProps)
                {
                    
                }
            }
            
        }
    }
}
