﻿using uLocate.Core;
using uLocate.Core.Extensions;
using uLocate.Core.Models;
using uLocate.Core.Services;
using umbraco.interfaces;

namespace uLocate.Web
{
    public class GeocodedAddressTasks : ITaskReturnUrl
    {
        private int _parentID;
        private int _userID;
        private string _returnUrl = "";

        #region ITaskReturnUrl Members

        public string ReturnUrl
        {
            get { return _returnUrl; }
        }

        #endregion

        #region ITask Members

        public int UserId { set { _userID = value; } }

        public int TypeID { get; set; }

        public string Alias { get; set; }

        public int ParentID
        {
            set
            {
                _parentID = value;
                if (_parentID == 1)
                    _parentID = -1;
            }
            get
            {
                return _parentID;
            }
        }

        public bool Save()
        {
            var address = new GeocodedAddress()
            {
                Name = Alias,
                CountryCode = string.IsNullOrEmpty(Constants.Configuration.DefaultCountryCode) ? string.Empty : Constants.Configuration.DefaultCountryCode
            };
            address.Save();

            _returnUrl = string.Format("{0}?id={1}", Constants.Urls.GeocodedAddressEditPage, address.Id);

            return true;
        }

        public bool Delete()
        {            

            var address = ULocateHelper.GeocodedAddress(ParentID);
            if (address != null)
            {
                var service = new GeocodedAddressService();
                service.Delete(address);
            }                

            return true;
        }

        #endregion
    }
}