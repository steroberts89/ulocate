﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace uLocate.Web.Extensions
{
    public static class UserControlExtentions
    {
        public static bool IsEmpty(this TextBox textbox)
        {
            return string.IsNullOrEmpty(textbox.Text);
        }

        public static bool HasValue(this TextBox textbox)
        {
            return !textbox.IsEmpty();
        }
    }
}