﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Xml.Linq;
//using Kent.Boogaart.KBCsv;
using ServiceStack.Text;
using uLocate.Core;
using uLocate.Core.Extensions;
using uLocate.Core.Models;

namespace uLocate.Web.UserControls
{
    public partial class ImportAddressData : UserControl
    {
        private static Coordinate defaultCoordinate = new Coordinate(0, 0);
        private static Viewport defaultViewport = new Viewport(defaultCoordinate, new Coordinate(0, 1));

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            var fileName = fuImport.FileName;

            if (!string.IsNullOrEmpty(fileName))
            {
                var addresses = new List<GeocodedAddress>();

                switch (GetFileExtension(fileName))
                {
                    case "CSV":
                        addresses = GetGeocodedAddressFromCsv(new StreamReader(fuImport.FileContent));
                        break;

                    case "JSON":
                        // TODO : This does not deserialize the Coordinate or Viewport correctly
                        addresses = JsonSerializer.DeserializeFromStream<List<GeocodedAddress>>(fuImport.FileContent);

                        foreach (var adr in addresses)
                        {
                            // set the id to 0 to assert this is a new GeocodedAddress
                            adr.Id = 0;

                            // Geography data cannot be null for PetaPoco - so force the Geocode to execute
                            adr.GeocodeStatus = GeocodeStatus.NOT_QUERIED;
                        }

                        break;

                    case "XML":
                        addresses = GetGeocodedAddressesFromXml(XDocument.Load(fuImport.FileContent));
                        break;
                }

                // Hide the form element placeholder
                this.phImport.Visible = false;

                var importLimit = Constants.Configuration.Providers["GeocodingAPIProvider"].GeocodeLimit;

                // Compare the number of addresses to be imported where GeocodeStatus is NOT_QUERIED
                // to protect against having to do a lot of manual saving/resaving after provider limit is passed
                if (addresses.Where(x => x.GeocodeStatus == GeocodeStatus.NOT_QUERIED).Count() > importLimit)
                {
                    this.ltrlFeedback.Text = string.Concat("The import limit for the current Geocoding provider is currently ", importLimit, ".  There are too many addresses to geocode in a single import.");
                }
                else
                {
                    int importCount = 0;

                    foreach (var adr in addresses)
                    {
                        adr.Save();
                        importCount++;
                    }

                    this.ltrlFeedback.Text = string.Concat("Imported ", importCount, " successfully!");
                }

                // display feedback
                this.phFeedback.Visible = true;
            }
        }

        // TODO : Think about moving some flavor of these 'Parsing' methods into uLocate.Core
        #region Parsing

        /// <summary>
        /// Parses the CSV stream - this method will for a geocode on all entries
        /// </summary>
        /// <remarks>Dependency Kent.Boogaart.KBCsv - acquired via NuGet</remarks>
        private List<GeocodedAddress> GetGeocodedAddressFromCsv(TextReader stream)
        {
            var addresses = new List<GeocodedAddress>();

            //using (var reader = new CsvReader(stream))
            //{
            //    // the CSV file is required to have a header row
            //    var header = reader.ReadHeaderRecord();

            //    while (reader.HasMoreRecords)
            //    {
            //        var row = reader.ReadDataRecord();

            //        // the ServiceStack.Text exports are using the "JSV" format ... so we will respect that to make import/export 
            //        // portable http://www.servicestack.net/docs/text-serializers/json-csv-jsv-serializers

            //        var geocodeStatus = GeocodeStatus.NOT_QUERIED;

            //        var address = new GeocodedAddress()
            //            {
            //                Name = row["Name"],
            //                Address1 = row.GetValueOrNull("Address1") == null ? string.Empty : row.GetValueOrNull("Address1"),
            //                Address2 = row.GetValueOrNull("Address2") == null ? string.Empty : row.GetValueOrNull("Address2"),
            //                Locality = row.GetValueOrNull("Locality") == null ? string.Empty : row.GetValueOrNull("Locality"),
            //                Region = row.GetValueOrNull("Region") == null ? string.Empty : row.GetValueOrNull("Region"),
            //                PostalCode = row.GetValueOrNull("PostalCode") == null ? string.Empty : row.GetValueOrNull("PostalCode"),
            //                CountryCode = row.GetValueOrNull("CountryCode") == null ? string.Empty : row.GetValueOrNull("CountryCode"),
            //                Telephone = row.GetValueOrNull("Telephone") == null ? string.Empty : row.GetValueOrNull("Telephone"),
            //                Fax = row.GetValueOrNull("Fax") == null ? string.Empty : row.GetValueOrNull("Fax"),
            //                WebsiteUrl = row.GetValueOrNull("WebsiteUrl") == null ? string.Empty : row.GetValueOrNull("WebsiteUrl"),
            //                Email = row.GetValueOrNull("Email") == null ? string.Empty : row.GetValueOrNull("Email"),
            //                Comment = row.GetValueOrNull("Comment") == null ? string.Empty : row.GetValueOrNull("Comment"),
            //                Coordinate = defaultCoordinate,
            //                Viewport = defaultViewport,
            //                GeocodeStatus = geocodeStatus
            //            };

            //        addresses.Add(address);
            //    }
            //}

            return addresses;
        }

        /// <summary>
        /// Parses an XDocument of GeocodedAddress and returns a List<GeocodedAddress>
        /// </summary>             
        private List<GeocodedAddress> GetGeocodedAddressesFromXml(XDocument xDoc)
        {
            var addresses = new List<GeocodedAddress>();
            var ns = xDoc.Root.Name.Namespace;

            foreach (var el in xDoc.Descendants().Where(x => x.Name == ns + "GeocodedAddress"))
            {
                var geocodeStatus = GeocodeStatus.NOT_QUERIED;

                // Coordinate
                var coordinate = defaultCoordinate;

                if (HasXElementChildren(el, ns, "Coordinate"))
                {
                    coordinate = new Coordinate(
                        double.Parse(el.Element(ns + "Coordinate").Element(ns + "latitude").Value),
                        double.Parse(el.Element(ns + "Coordinate").Element(ns + "longitude").Value)
                        );

                    geocodeStatus = GeocodeStatus.OK;
                }

                // Viewport
                var viewport = defaultViewport;

                if (HasXElementChildren(el, ns, "Viewport"))
                {
                    var vpEl = el.Element(ns + "Viewport");

                    viewport = new Viewport(
                                    new Coordinate(
                                        double.Parse(vpEl.Element(ns + "northeast").Element(ns + "latitude").Value),
                                        double.Parse(vpEl.Element(ns + "northeast").Element(ns + "longitude").Value)
                                        ),
                                    new Coordinate(
                                        double.Parse(vpEl.Element(ns + "southwest").Element(ns + "latitude").Value),
                                        double.Parse(vpEl.Element(ns + "southwest").Element(ns + "longitude").Value)
                                    ));
                }

                var address = new GeocodedAddress()
                {
                    Name = el.Element(ns + "Name").Value,
                    Address1 = HasXElementChildren(el, ns, "Address1") ? el.Element(ns + "Address1").Value : string.Empty,
                    Address2 = HasXElementChildren(el, ns, "Address2") ? el.Element(ns + "Address2").Value : string.Empty,
                    Locality = HasXElementChildren(el, ns, "Locality") ? el.Element(ns + "Locality").Value : string.Empty,
                    Region = HasXElementChildren(el, ns, "Region") ? el.Element(ns + "Region").Value : string.Empty,
                    PostalCode = HasXElementChildren(el, ns, "PostalCode") ? el.Element(ns + "PostalCode").Value : string.Empty,
                    CountryCode = HasXElementChildren(el, ns, "CountryCode") ? el.Element(ns + "CountryCode").Value : string.Empty,
                    Telephone = HasXElementChildren(el, ns, "Telephone") ? el.Element(ns + "Telephone").Value : string.Empty,
                    Fax = HasXElementChildren(el, ns, "Fax") ? el.Element(ns + "Fax").Value : string.Empty,
                    WebsiteUrl = HasXElementChildren(el, ns, "WebsiteUrl") ? el.Element(ns + "WebsiteUrl").Value : string.Empty,
                    Email = HasXElementChildren(el, ns, "Email") ? el.Element(ns + "Email").Value : string.Empty,
                    Comment = HasXElementChildren(el, ns, "Comment") ? el.Element(ns + "Comment").Value : string.Empty,
                    Coordinate = coordinate,
                    Viewport = viewport,
                    GeocodeStatus = geocodeStatus
                };

                addresses.Add(address);
            }

            return addresses;
        }

        #endregion

        /// <summary>
        /// Determines if a child XElement with a given name exists
        /// </summary>
        private bool HasXElementChildren(XElement el, XNamespace ns, string name)
        {
            return el.Elements().Where(x => x.Name == ns + name).Count() > 0;
        }

        /// <summary>
        /// Returns the extension of the fileName passed
        /// </summary>
        private string GetFileExtension(string fileName)
        {
            var dotIndex = fileName.LastIndexOf('.') + 1;
            return fileName.Substring(dotIndex, fileName.Length - dotIndex).ToUpper();

        }
    }
}