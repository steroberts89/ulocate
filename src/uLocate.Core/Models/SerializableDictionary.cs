﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace uLocate.Core.Models
{
    [DataContract]
    internal class SerializableDictionary
    {
        [DataMember]
        public Dictionary<string, string> Dictionary { get; internal set; }

        public SerializableDictionary(IDictionary<string, string> dictionary)
        {
            Dictionary = (Dictionary<string, string>)dictionary;
        }
    }
}
