﻿namespace uLocate.Core.Models
{
    /// <summary>
    /// Enumeration of the Google Geocoding API "location_type".
    /// </summary>
    /// <remarks>
    /// Yahoo! PlaceFinder uses the term Quality and has a much larger enumeration.
    /// </remarks>
    public enum GeocodeQuality
    {
        ROOFTOP,
        RANGE_INTERPOLATED,
        GEOMETRIC_CENTER,
        APPROXIMATE,
        NONE
    }

    /// <summary>
    /// Enumeration of Statuses taken directly from the Google Geocoding API
    /// https://developers.google.com/maps/documentation/geocoding/#XML
    /// </summary>
    public enum GeocodeStatus
    {
        OK,
        ZERO_RESULTS,
        OVER_QUERY_LIMIT,
        REQUEST_DENIED,
        INVALID_REQUEST,
        UNKNOWN_ERROR,
        NOT_QUERIED
    }
}