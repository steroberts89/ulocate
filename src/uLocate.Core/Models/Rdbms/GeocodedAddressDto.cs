﻿using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
using Microsoft.SqlServer.Types;
using System;

namespace uLocate.Core.Models.Rdbms
{
    [TableName("ulocateGeocodedAddress")]
    [PrimaryKey("id", autoIncrement = true)]
    [ExplicitColumns]
    public class GeocodedAddressDto
    {
        [Column("id")]
        [PrimaryKeyColumn]
        public int Id { get; set; }

        [Column("name")]
        [Length(255)]
        public string Name { get; set; }

        [Column("address1")]
        [Length(255)]
        public string Address1 { get; set; }

        [Column("address2")]
        [Length(255)]
        public string Address2 { get; set; }

        [Column("locality")]
        [Length(50)]
        public string Locality { get; set; }

        [Column("region")]
        [Length(50)]
        public string Region { get; set; }

        [Column("postalCode")]
        [Length(50)]
        public string PostalCode { get; set; }

        [Column("countryCode")]
        [Length(2)]
        public string CountryCode { get; set; }

        [Column("comment")]
        [Length(500)]
        public string Comment { get; set; }

        [Column("coordinateGeography")]
        public SqlGeography CoordinateGeography { get; set; }

        [Column("viewportGeography")]
        public SqlGeography ViewportGeography { get; set; }

        [Column("geoCodeStatus")]
        public string GeoCodeStatus { get; set; }

        [Column("extendedDataXml")]
        public string ExtendedDataXml { get; set; }

        [Column("createDate")]
        [Constraint(Default = "getdate()")]
        public DateTime CreateDate { get; set; }

        [Column("updateDate")]
        [Constraint(Default = "getdate()")]
        public DateTime UpdateDate { get; set; }

    }
}
