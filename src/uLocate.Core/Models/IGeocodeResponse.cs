﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uLocate.Core.Models
{
    public interface IGeocodeResponse 
    {        
        /// <summary>
        /// The API returned status translated
        /// </summary>
        GeocodeStatus Status { get; }

        /// <summary>
        /// Collection of geocode results
        /// </summary>
        /// <remarks>
        /// Google - Placemark | Yahoo! - Result
        /// </remarks>
        IEnumerable<IGeocode> Results { get; }
    }
}
