﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uLocate.Core.Persistence.Repositories;
using Umbraco.Core.Persistence.UnitOfWork;

namespace uLocate.Core.Persistence
{
    /// <summary>
    /// Used to instantiate each repository type
    /// </summary>
    public class RepositoryFactory
    {
        public virtual IGeocodedAddressRepository CreateGeocodedAddressRepository(IDatabaseUnitOfWork uow)
        {
            return new GeocodedAddressRepository(uow);
        }
    }
}
