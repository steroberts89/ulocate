﻿using System.Configuration;

namespace uLocate.Core.Configuration
{
    public class uLocateSection : ConfigurationSection
    {
        /// <summary>
        /// Gets/Sets the default connectionstring name for uLocate database connectivity
        /// </summary>
        [ConfigurationProperty("defaultConnectionStringName", DefaultValue = "umbracoDbDSN", IsRequired = false)]
        public string DefaultConnectionStringName
        {
            get { return (string)this["defaultConnectionStringName"]; }
            set { this["defaultConnectionStringName"] = value; }
        }

        /// <summary>
        /// Gets/Sets teh default country code, primarily used for UI controls
        /// </summary>
        [ConfigurationProperty("defaultCountryCode", IsRequired=false)]
        public string DefaultCountryCode
        {
            get { return (string)this["defaultCountryCode"]; }
            set { this["defaultCountryCode"] = value; }
        }

        /// <summary>
        /// Gets/Sets the enableLogging property setting
        /// </summary>
        [ConfigurationProperty("enableLogging", DefaultValue=false, IsRequired=false)]
        public bool EnableLogging
        {
            get { return (bool)this["enableLogging"]; }
            set { this["enableLogging"] = value; }
        }

        /// <summary>
        /// Gets the providers configuration collection
        /// </summary>
        [ConfigurationProperty("providers", IsRequired=false), ConfigurationCollection(typeof(ProviderCollection), AddItemName="provider")]
        public ProviderCollection Providers
        {
            get { return (ProviderCollection)this["providers"]; }
        }

        [ConfigurationProperty("datatypes", IsRequired=false), ConfigurationCollection(typeof(ProviderCollection), AddItemName="datatype")]
        public DataTypeCollection DataTypes
        {
            get { return (DataTypeCollection)this["datatypes"]; }
        }

        ///// <summary>
        ///// Gets the APIs configuration collection
        ///// </summary>
        //[ConfigurationProperty("APIs", IsRequired=false), ConfigurationCollection(typeof(ProviderCollection), AddItemName="datatype")]
        //public APICollection APIs
        //{
        //    get { return (APICollection)this["apis"]; }
        //}
    }
}
