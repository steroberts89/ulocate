using System;
using System.Linq;
using System.Text;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;


namespace uLocate.Core.Models.Rdbms
{
    [TableName("ulocateContactInfo")]
    [PrimaryKey("id", autoIncrement = true)]
    [ExplicitColumns]
    public class ContactInfo
    {
        [Column("id")]
        [PrimaryKeyColumn]
        public int Id { get; set; }

        [Column("name")]
        [Length(255)]
        public string Name { get; set; }

        [Column("contactType")]
        [Length(50)]
        public string ContactType { get; set; }

        [Column("value")]
        [Length(255)]
        public string Value { get; set; }

        [Column("geoAddressId")]
        [ForeignKey(typeof(GeocodedAddressDto))]
        [Index(IndexTypes.NonClustered, Name = "")]
        public int GeocodedAddressId { get; set; }

        [Column("createDate")]
        [Constraint(Default = "getdate()")]
        public DateTime CreateDate { get; set; }

        [Column("updateDate")]
        [Constraint(Default = "getdate()")]
        public DateTime UpdateDate { get; set; }

        [ResultColumn]
        public GeocodedAddressDto GeocodedAddressDto { get; set; }
    }
}
