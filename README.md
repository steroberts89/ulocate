uLocate
=========

uLocate is a collaborative Umbraco package which aims to provide Geography data types for Umbraco based on the Microsoft SQL Server Geography type.

This package will only work using a SQL Server database and requires that SQL CLR Types have been installed on the web server.
